const actionHandlers = {
    'ADD_POST': (state, action) => state.concat(action.data),
    'DELETE_POST': (state, action) => {
        return state.filter(post => post.id !== action.id);
    }
}

const postReducer =  (state = [], action) => {
    const actionHandler = actionHandlers[action.type];

    if (actionHandler) {
      return actionHandler(state, action);
    }

    return state;
}

export default postReducer;
